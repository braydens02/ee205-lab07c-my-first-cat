///////////////////////////////////////////////////////////////////////////////
///          University of Hawaii, College of Engineering
/// @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello1.cpp
/// @version 1.0 - Initial implementation
///
/// Build and test 3 Hello World Programs
///
/// @author  @author Brayden Suzuki <braydens@hawaii.edu>
/// @@date   Feb 27 2022
///
/// @see     https://www.gnu.org/software/make/manual/make.html
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
using namespace std;

int main() {
   cout << "Hello World!" << endl ;
   return 0;
}
